#!/usr/bin/env python
import roslib
import rospy
import numpy as np
from numpy import linalg as LA
import numpy
import os
import sys
from std_msgs.msg import Float64;
import math
import matplotlib.pyplot as plt
from contracts.library.array import shape
#get the raw data 
raw_data = np.matrix([[3.2501, 2.6075, 1.2908, 0.7976, 0.1969, 0.2809, 0.3654, 0.5942, 0.6236, 1.2764, 2.5247, 1.1733, 1.0354, -0.3026],
                      [-0.891, -0.8910, -0.8910, -0.8910, 0.1090, 0.1090, 0.1090, 0.1090, 0.1090, 0.1090, 0.1090, 1.1090, 1.1090, 1.1090]])
dimOne = np.size(raw_data,0)
dimTwo = np.size(raw_data,1)
data = np.zeros(shape=(dimOne,dimTwo))
#compute the mean of the data
meanX = np.mean(raw_data[0,:])
meanY = np.mean(raw_data[1,:])
#subtract the mean from the raw data
for i in range(raw_data[0,:].size):
    data[0,i] = raw_data[0,i] - meanX

for j in range(raw_data[1,:].size):
    data[1,j] = raw_data[1,j] - meanY
    
meanXsub = np.mean(data[0,:]) # the new mean after subtraction should be zero
meanYsub = np.mean(data[1,:]) # the new mean after subtraction should be zero
#compute the 2*2 covariance matrix
cov_x_y = np.cov(data)
#compute the eigenvalues and eignenvectors of the covariance matrix
w, v = LA.eig(cov_x_y)
#find the maximum eigenvalue and the corresponding eigenvector
max_eigen_value = max(w)
max_eigen_value_index = [index for index,value in enumerate(w) if value==max(w)]
eigen_vector = v[:,max_eigen_value_index].transpose()

#get points
#PCA vector points
x = [3*eigen_vector[0,0],-3*eigen_vector[0,0]]
y = [3*eigen_vector[0,1],-3*eigen_vector[0,1]]
#x-axis
xxaxis = [-5,5]
xyaxis = [0,0]
#y-axis
yxaxis = [0,0]
yyaxis = [-5,5]

# compute the angle betweem the PCA vector and x-axis
thet = np.arctan2(eigen_vector[0,1], eigen_vector[0,0]) * 180 / np.pi + 180
print thet

# plot 
plt
plt.plot(xxaxis,xyaxis,'r--') #plot x axis
plt.plot(yxaxis,yyaxis,'r--') #plot y axis
plt.plot(x,y,'b-', linewidth=3.0) #plot the PCA vector
plt.plot(data[0,:],data[1,:],'ro') #plot the data
plt.plot(x,y,linewidth=3.0) #plot the PCA vector
plt.axis([-4, 4, -4, 4])
plt.show()

