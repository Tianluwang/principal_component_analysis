#!/usr/bin/env python
import roslib
import rospy
import numpy as np
from numpy import linalg as LA
import numpy
import os
import sys
from std_msgs.msg import Float64;
from std_msgs.msg import Float64MultiArray;
import math
import matplotlib.pyplot as plt
from contracts.library.array import shape

class principal_component_analysis(object):
    def __init__(self):
        self.node_name = rospy.get_name()
        #initialize the subscriber & publisher
        self.top_local_esimation_array = rospy.Subscriber("huskanypulator/top_local_esimation_array", Float64MultiArray, self.callback, queue_size=1)
        self.pub_target_orientation = rospy.Publisher("principal_component_analysis/target_orientation", Float64, queue_size=1)
    def callback(self, msg):
        #get the measurements from huskanypulator_grasping_controller
        tuple_data = msg.data
        #transform the tuple data list type to array
        trans_data = np.asarray(tuple_data)
        #reshape the data into three rows
        three_row_data = np.reshape(trans_data,(3,-1))
        #only use the first two row data
        raw_data = three_row_data[[0,1],:]
        #define a zero data array
        dimOne = np.size(raw_data,0)
        dimTwo = np.size(raw_data,1)
        data = np.zeros(shape=(dimOne,dimTwo))
        #compute the mean of the data
        meanX = np.mean(raw_data[0,:])
        meanY = np.mean(raw_data[1,:])
        #subtract the mean from the raw data
        for i in range(raw_data[0,:].size):
            data[0,i] = raw_data[0,i] - meanX
        
        for j in range(raw_data[1,:].size):
            data[1,j] = raw_data[1,j] - meanY
            
        meanXsub = np.mean(data[0,:]) # the new mean after subtraction should be zero
        meanYsub = np.mean(data[1,:]) # the new mean after subtraction should be zero
        #compute the 2*2 covariance matrix
        cov_x_y = np.cov(data)
        #compute the eigenvalues and eignenvectors of the covariance matrix
        w, v = LA.eig(cov_x_y)
        #find the maximum eigenvalue and the corresponding eigenvector
        max_eigen_value = max(w)
        max_eigen_value_index = [index for index,value in enumerate(w) if value==max(w)]
        eigen_vector = v[:,max_eigen_value_index].transpose()
        
        #compute the angle betweem the PCA vector and x-axis
        thet = np.arctan2(eigen_vector[0,1], eigen_vector[0,0]) * 180 / np.pi + 180        
        #publish the computed target orientation direction
        self.publishOrient(thet)
        print thet
    def publishOrient(self, target_thet):
        self.pub_target_orientation.publish(target_thet)
if __name__ == '__main__':
    # initialize the node with rospy
    rospy.init_node('principal_component_analysis', anonymous=False)
    # create the object
    principal_component_analysis = principal_component_analysis()
    try:
        rospy.spin()   
    except rospy.ROSInterruptException:
        print("Shutting down!!!")
        pass
