#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String 
from std_msgs.msg import Float64MultiArray;

def talker():
    pub = rospy.Publisher("principal_component_analysis/top_local_esimation_array", Float64MultiArray, queue_size=1)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        
        raw_data = [3.2501, 2.6075, 1.2908, 0.7976, 0.1969, 0.2809, 0.3654, 0.5942, 0.6236, 1.2764, 2.5247, 1.1733, 1.0354, -0.3026]
        rospy.loginfo(raw_data)
        pub.publish(raw_data)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass