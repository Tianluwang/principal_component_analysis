# principal_component_analysis

Author(s): Tianlu Wang

Maintainer(s): Tianlu Wang ([tiawang@ethz.ch](mailto:tiawang@ethz.ch)), Martin Wermelinger ([martiwer@ethz.ch](mailto:martiwer@ethz.ch))

This package is used to find the principal component of the object's direction. The node can be launched by 
```
#!bash
roslaunch principal_component_analysis principal_component_analysis.launch

```
